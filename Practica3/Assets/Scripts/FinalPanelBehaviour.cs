﻿using UnityEngine;
using UnityEngine.UI;

public class FinalPanelBehaviour : MonoBehaviour
{
    Animator epanim = null;
    // Clase que representa el panel de fin de juego
    void Start()
    {
        epanim = GetComponent<Animator>();        
    }
    public void appear()
    {
        epanim.SetTrigger("Appear");
    }
    public void reset()
    {
        epanim.SetTrigger("Reset");
        epanim.Play("Idle");
        epanim.ResetTrigger("Appear");
        epanim.ResetTrigger("Reset");
    }
}
