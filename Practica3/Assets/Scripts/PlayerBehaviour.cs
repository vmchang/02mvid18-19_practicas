﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    // Definir Hashes de:
    // Parametros (Speed, Attack, Damage, Dead)
    // Estados (Base Layer.Idle, Attack Layer.Idle, Attack Layer.Attack)
    static int BaseIdleHash = Animator.StringToHash("Base Layer.Idle");
    static int AttackIdleHash = Animator.StringToHash("Attack Layer.Idle");
    static int AttackAttHash = Animator.StringToHash("Attack Layer.Attack");
    static int AttackHash = Animator.StringToHash("Attack");
    static int DeadHash = Animator.StringToHash("Dead");
    static int DmgHash = Animator.StringToHash("Damage");
    static int SpeedHash = Animator.StringToHash("Speed");

    //Componentes de Audio
    public AudioSource audiosrc;
    public AudioClip attclip = null;
    public AudioClip dmgclip = null;
    public AudioClip deadclip = null; 

    // Componente Animator, RigidBody publico

    public Animator anim = null;
    public Rigidbody rb = null;
    public BoxCollider box;

    public float walkSpeed		= 1;		// Parametro que define la velocidad de "caminar"
	public float runSpeed		= 1;		// Parametro que define la velocidad de "correr"
	public float rotateSpeed	= 160;		// Parametro que define la velocidad de "girar"

	// Variables auxiliares
	float _angularSpeed			= 0;		// Velocidad de giro actual
	float _speed				= 0;		// Velocidad de traslacion actual
	float _originalColliderZ	= 0;		// Valora original de la posición 'z' del collider

	// Variables internas:
	int _lives = 3;							// Vidas restantes
	public bool paused = false;				// Indica si el player esta pausado (congelado). Que no responde al Input

    void Start()
	{
        // Obtener los componentes Animator, Rigidbody y el valor original center.z del BoxCollider
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        box = GetComponent<BoxCollider>();
        _originalColliderZ = box.center.z;
        audiosrc = GetComponent<AudioSource>();

    }

	// Aqui moveremos y giraremos la araña en funcion del Input
	void FixedUpdate()
	{
        // Si estoy en pausa no hacer nada (no moverme ni atacar)
        if (paused) { return; }

        // Calculo de velocidad lineal (_speed) y angular (_angularSpeed) en función del Input
        // Si camino/corro hacia delante delante: _speed = walkSpeed   /  _speed = runSpeed
        if (Input.GetKey(KeyCode.UpArrow) || CrossButton.GetInput(InputType.UP)) { 
            _speed = (Input.GetKey(KeyCode.LeftShift) || GamePadButton.GetInput(InputType2.R)) ? runSpeed : walkSpeed;
        }
        // Si camino/corro hacia delante detras: _speed = -walkSpeed   /  _speed = -runSpeed
        if (Input.GetKey(KeyCode.DownArrow) || CrossButton.GetInput(InputType.DOWN)) {
            _speed = (Input.GetKey(KeyCode.LeftShift) || GamePadButton.GetInput(InputType2.A)) ? -runSpeed : -walkSpeed;
        }

        // Si no me muevo: _speed = 0
        if ((Input.GetKey(KeyCode.UpArrow) == false && Input.GetKey(KeyCode.DownArrow) == false ) 
           && (CrossButton.GetInput(InputType.UP) == false && CrossButton.GetInput(InputType.DOWN) == false)) {
            _speed = 0;
        }

        // Si giro izquierda: _angularSpeed = -rotateSpeed;
        if (Input.GetKey(KeyCode.LeftArrow) || CrossButton.GetInput(InputType.LEFT)) {
            _angularSpeed = -rotateSpeed;
        }

        // Si giro derecha: _angularSpeed = rotateSpeed;
        if (Input.GetKey(KeyCode.RightArrow) || CrossButton.GetInput(InputType.RIGHT)) {
            _angularSpeed = rotateSpeed;
        }

        // Si no giro : _angularSpeed = 0;
        if ((Input.GetKey(KeyCode.LeftArrow) == false && Input.GetKey(KeyCode.RightArrow) == false) 
           && (CrossButton.GetInput(InputType.LEFT) == false && CrossButton.GetInput(InputType.RIGHT) == false)) {
            _angularSpeed = 0;
        }

        // Actualizamos el parámetro "Speed" en función de _speed. Para activar la anicación de caminar/correr
        anim.SetFloat(SpeedHash, _speed);
        // Movemos y rotamos el rigidbody (MovePosition y MoveRotation) en función de "_speed" y "_angularSpeed"
        rb.MovePosition(transform.position + (transform.forward * Time.deltaTime * 2 * _speed));
        rb.MoveRotation(transform.rotation * Quaternion.Euler(transform.up * Time.deltaTime * 2 * _angularSpeed));
        
        // Mover el collider en función del parámetro "Distance" (necesario cuando atacamos)
        box.center = new Vector3(box.center.x, box.center.y, _originalColliderZ + (anim.GetFloat("Distance") * 14));
    }

	// En este bucle solamente comprobaremos si el Input nos indica "atacar" y activaremos el trigger "Attack"
	private void Update()
	{
        // Si estoy en pausa no hacer nada (no moverme ni atacar)
        if (paused) return;

        // Si detecto Input tecla/boton ataque ==> Activo disparados 'Attack'
       if (Input.GetKey(KeyCode.Space) || GamePadButton.GetInput(InputType2.A)) {
            anim.SetTrigger(AttackHash);
            audiosrc.clip = attclip;
            audiosrc.PlayDelayed(0.5f);
       }

	}

	// Función para resetear el Player
	public void reset()
	{
        //Reiniciar el numero de vidas
        _lives = 3;
        UIManager.instance.updateLives(_lives);
        // Pausamos a Player
        paused = true;

        // Forzar estado Idle en las dos capas (Base Layer y Attack Layer): función Play() de Animator
        anim.Play(AttackIdleHash);
        anim.Play(BaseIdleHash);

        // Reseteo todos los triggers (Attack y Dead)
        anim.ResetTrigger(AttackHash);
        anim.ResetTrigger(DeadHash);
        

        // Posicionar el jugador en el (0,0,0) y rotación nula (Quaternion.identity)
        rb.MovePosition(new Vector3(9f, 0f, -8.7f)); //posicion de inicio elegida en vez del (0,0,0) sale de la cueva
        Debug.Log("Position reset to: " + rb.transform.position);
        rb.rotation = (Quaternion.identity);
        rb.MoveRotation(Quaternion.Euler(0f,-54f, 0f)); //posicion de rotacion elegida para que apunte a la salida de la cueva
        Debug.Log("Rotation reset to: " + rb.transform.rotation);
    }

	// Funcion recibir daño
	public void recieveDamage()
	{
        // Restar una vida
        // Si no me quedan vidas notificar al GameManager (notifyPlayerDead) y disparar trigger "Dead"
        Debug.Log("Player Hitted");
		if (_lives > 0) {
            _lives -= 1;
            Debug.Log("Lives remaining: "+_lives);
            // Si aun me quedan vidas dispara el trigger TakeDamage
            audiosrc.clip = dmgclip;
            audiosrc.PlayDelayed(0.3f);
            anim.SetTrigger(DmgHash);
            UIManager.instance.updateLives(_lives);
        }
        if (_lives == 0)
        {
            audiosrc.clip = deadclip;
            audiosrc.PlayDelayed(0f);
            anim.SetTrigger(DeadHash);
            GameManager.instance.notifyPlayerDead();
        }

	}

	private void OnCollisionEnter(Collision collision)
	{
        // Obtener estado actual de la capa Attack Layer
        AnimatorStateInfo infoState = anim.GetCurrentAnimatorStateInfo(1);

        // Si el estado es 'Attack' matamos al enemigo (mirar etiqueta)
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Enemy hitted");
            if (infoState.fullPathHash == AttackAttHash && anim.GetFloat("Distance") > 0)
            {
                Debug.Log("Enemy killed");
                collision.gameObject.GetComponent<SkeletonBehaviour>().kill();
            }
        }
    }
}

