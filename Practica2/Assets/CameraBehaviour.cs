﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    /*Variables publicas */
    public float MinHeight = 4.0f;
    public float CameraHeightFactor = 1.5f;
    public Transform Target;

    /*Variables privadas*/
    private float Height;
    private Vector3 offset;
    //// Start is called before the first frame update
    void Start()
    {
        Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        Height = 0f; ;
        offset = new Vector3(0f, Height, 0f);
        Debug.Log("Camera script start");
    }

    // Se ejecuta después de todos los Update().
    void LateUpdate()
    {   if (GameObject.Find("Player").activeSelf) { 
            Debug.Log("Running camera update");
            if  (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.DownArrow)) {
                Height = MinHeight * (1 + GameObject.Find("Player").GetComponent<Rigidbody>().velocity.magnitude / CameraHeightFactor); ;
                offset.y = Height;
                transform.position = Target.transform.position + offset;
            }
            else
            {
                Height = MinHeight;
                offset.y = Height;
                transform.position = Target.transform.position + offset;
            }
        }
    }
}
