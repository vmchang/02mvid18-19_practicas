﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	#region SINGLETON
	protected static UIManager _instance = null;
	public static UIManager instance { get { return _instance; } }
	void Awake() { _instance = this; }
	#endregion

	// Menu principal
	public GameObject mainMenu			= null;	// Panel del menu principal (Primera pantalla en mostrarse)

	// Sub-menus durante el juego
	public FinalPanelBehaviour endPanel	= null;	// Panel de fin de juego (Dentro de la interfaz del juego)
	public Text scoreText				= null;	// Puntuacion del juego


	public void showMainMenu()
	{
		// Mostrar objeto mainMenu
		mainMenu.SetActive(true);

        // Ocultar endPanel
        endPanel.gameObject.SetActive(false);
	}

	public void hideMainMenu()
	{
        // Ocultar objeto mainMenu
        mainMenu.SetActive(false);
	}

	public void showEndPanel(bool win)
	{
		// Mostrar panel fin de juego (ganar o perder)
		if (win)
        {
            endPanel.gameObject.GetComponentInChildren<Text>().text = "Has ganado! - Fin del Juego";
            endPanel.appear();
        }
        else
        {
            endPanel.gameObject.GetComponentInChildren<Text>().text = "Perdiste!! - Fin del Juego";
            endPanel.appear();
        }
	}

	public void hideEndPanel()
	{
        // Ocultar el panel
        endPanel.gameObject.SetActive(false);
    }

	public void updateScore(int score)
	{
        // Actualizar el 'UI text' con la puntuacion 
        scoreText.text = score.ToString();
	}

    public void updateLives(int lives)
    {
        switch (lives) {
            case 0:
                GameObject.Find("Heart1").SetActive(false); break;
            case 1:
                GameObject.Find("Heart2").SetActive(false); break;
            case 2:
                GameObject.Find("Heart3").SetActive(false); break;
            case 3:
                GameObject.Find("Heart1").SetActive(true);
                GameObject.Find("Heart2").SetActive(true);
                GameObject.Find("Heart3").SetActive(true); break;
        }
    }

}
