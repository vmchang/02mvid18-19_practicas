﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBehaviour : MonoBehaviour
{
    private bool BaseDetected;
    private Material[] cachedMaterials;
    // Start is called before the first frame update
    void Start()
    {
        BaseDetected = false;
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && BaseDetected == false)
        {
            //Cambiamos el color de emision de BaseNeon
            cachedMaterials = gameObject.GetComponent<Renderer>().materials;
            cachedMaterials[1].SetColor("_EmissionColor", Color.green);
            gameObject.GetComponent<Renderer>().materials = cachedMaterials;
            
            other.gameObject.GetComponent<PlayerBehavior>().BaseFounded();
            BaseDetected = true;
            Debug.Log("Vidas recargadas");
            other.gameObject.GetComponent<PlayerBehavior>().Vidas = 3;
            Debug.Log("Player: Vidas = " + other.gameObject.GetComponent<PlayerBehavior>().Vidas);
        }
        
    }
}
