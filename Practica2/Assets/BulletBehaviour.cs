﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public float speed;
    private Vector3 direction;
    private float lifeTime;
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        lifeTime = 1.5f;
        startTime = 0f;
    }

    public void SetDirection(Vector3 dir)
    {
        direction = dir;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (startTime < lifeTime) { 
            transform.Translate(direction * Time.deltaTime *speed);
        } else
        {
            gameObject.SetActive(false);
        }
        startTime += Time.deltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != tag) {
             if(other.tag == "Turret")
             {
                Debug.Log("Destruida unidad: " + other.gameObject.name);
                other.gameObject.SetActive(false);
                gameObject.SetActive(false);
             }
             if (other.tag == "Player")
             {
                other.GetComponent<PlayerBehavior>().Hit(gameObject);
                gameObject.SetActive(false);
             }
        }
    }

}
