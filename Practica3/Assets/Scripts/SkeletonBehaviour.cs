﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonBehaviour : MonoBehaviour
{
    // Definir Hashes de:
    // Parametros (Attack, Dead, Distance)
    // Estados (Attack, Idle)
    static int BaseAttackHash = Animator.StringToHash("Base Layer.Attack");
    static int IdleHash = Animator.StringToHash("Idle");
    static int AttackHash = Animator.StringToHash("Attack");
    static int DeadHash = Animator.StringToHash("Dead");

    //Componentes de Audio
    public AudioSource audiosrc = null;
    public AudioClip attclip = null;
    public AudioClip deadclip = null;

    //Componentes Animator, BoxCollider
    public Animator skanim = null;
    public BoxCollider skbox = null;

    // Variables auxiliares 
    PlayerBehaviour _player = null;     //Puntero a Player (establecido por método 'setPlayer')
    bool _dead = false; // Indica si ya he sido eliminado
    float _originalColliderZ = 0;        // Valora original de la posición 'z' del collider
    float _timeToAttack = 0;        // Periodo de ataque

    public void setPlayer(PlayerBehaviour player)
    {
        _player = player;
    }

    void Start()
    {
        // Obtener los componentes Animator y el valor original center.z del BoxCollider
        skanim = GetComponent<Animator>();
        skbox = GetComponent<BoxCollider>();
        _originalColliderZ = skbox.center.z;
        _timeToAttack = Time.deltaTime;
        audiosrc = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        // Si estoy muerto ==> No hago nada
        if (_dead) return;

        // Si Player esta a menos de 1m de mi y no estoy muerto:
        // - Le miro
        // - Si ha pasado 1s o más desde el ultimo ataque ==> attack()
        if (!_dead)
        {
            if ((gameObject.transform.position - _player.transform.position).sqrMagnitude < 2f)
            {
                transform.LookAt(_player.transform.position);
                if (_timeToAttack > 1f)
                {
                    attack();
                    _timeToAttack = 0f;
                    audiosrc.clip = attclip;
                    audiosrc.PlayDelayed(1f);
                }
                _timeToAttack += Time.deltaTime;
                Debug.Log(_timeToAttack);
            }
        }
        // Desplazar el collider en 'z' un multiplo del parametro Distance
        skbox.center = new Vector3(skbox.center.x, skbox.center.y, _originalColliderZ + (skanim.GetFloat("Distance") * 0.3f));
    }

    public void attack()
    {
        // Activo el trigger "Attack"
        skanim.SetTrigger(AttackHash);
    }

    public void kill()
    {
        // Guardo que estoy muerto, disparo trigger "Dead" y desactivo el collider
        _dead = true;
        audiosrc.clip = deadclip;
        audiosrc.PlayDelayed(0f);
        skanim.SetTrigger(DeadHash);
        skbox.enabled = false;
        
        // Notifico al GameManager que he sido eliminado
        GameManager.instance.notifyEnemyKilled(gameObject.GetComponent<SkeletonBehaviour>());
    }

    // Funcion para resetear el collider (activado por defecto), la variable donde almaceno si he muerto y forzar el estado "Idle" en Animator
    public void reset()
    {
        skbox.enabled = true;
        _dead = false;
        skanim.Play(IdleHash);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Obtener el estado actual
        AnimatorStateInfo infoState = skanim.GetCurrentAnimatorStateInfo(0);

        // Si el estado es 'Attack' y el parametro Distance es > 0 atacamos a Player (comprobar etiqueta).
        // La Distancia >0 es para acotar el ataque sólo al momento que mueve la espada (no toda la animación).
        if (collision.gameObject.tag == "Player")
        {
            if (infoState.fullPathHash == BaseAttackHash && skanim.GetFloat("Distance") > 0)
            {
                collision.gameObject.GetComponent<PlayerBehaviour>().recieveDamage();
            }

        }
    }
}