﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBehaviour : MonoBehaviour
{
    public Transform Canon;
    public Transform Gun;
    public Transform Turret;
    public Transform Target;
    public GameObject Bullet;
    public float FireDistance;
    public float FireRate;
    private float lastShot;
    private GameObject TurretBullet;

    // Start is called before the first frame update
    void Start()
    {
        FireDistance = 8f;
        FireRate = 1.5f;
        lastShot = 0f;
        Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        Canon = gameObject.GetComponentInChildren<Transform>().Find("Pylon");
        Turret = gameObject.GetComponentInChildren<Transform>().Find("Turret");
        Gun = gameObject.GetComponentInChildren<Transform>().Find("RotatingGuns");
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((Target.position - Canon.position).sqrMagnitude <= FireDistance){
            Gun.LookAt(Target, Canon.transform.up);
            Turret.LookAt(Target, Canon.transform.up);
            Canon.LookAt(Target, Canon.transform.up);

            if (lastShot >= FireRate) {
                Shot(Target);
            }
            lastShot += Time.deltaTime;
        }
    }

    public void Shot(Transform target)
    {
        lastShot = 0f;
        TurretBullet = Instantiate(Bullet);
        TurretBullet.GetComponent<BulletBehaviour>().tag = "Turret";
        TurretBullet.transform.position = Gun.position;
        TurretBullet.GetComponent<BulletBehaviour>().speed = 3.0f;
        TurretBullet.GetComponent<BulletBehaviour>().SetDirection(Gun.transform.forward);

    }
}
