﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour
{
    //Variables publicas
    public float Aceleration;
    public float TurnAceleration;
    public Rigidbody rb;
    public GameObject Bullet;
    public int Vidas;

    //Variables privadas
    private int BasesDescubiertas;
    private GameObject PlayerBullet;

    // Start is called before the first frame update
    void Start()
    {
        Vidas = 3;
        Debug.Log("Player: Vidas = " + Vidas);
        BasesDescubiertas = 0;
        Debug.Log("Player: Bases Descubiertas = " + BasesDescubiertas);
        rb = GetComponent<Rigidbody>();
        
        
    }

    public void Shoot() {
        PlayerBullet = Instantiate(Bullet);
        PlayerBullet.GetComponent<BulletBehaviour>().tag = "Player";
        PlayerBullet.transform.position = transform.position;
        PlayerBullet.GetComponent<BulletBehaviour>().speed = 4.0f;
        PlayerBullet.GetComponent<BulletBehaviour>().SetDirection(transform.forward);
        

    }

    public void Hit(GameObject other)
    {
        Vidas -= 1;
        if(Vidas <= 0)
        {
            Debug.Log("Player: Jugador destruido");
            gameObject.SetActive(false);
        } else
        {
            Debug.Log("Player: Daño recibido");
            Debug.Log("Player: Vidas = " + Vidas);
            rb.AddExplosionForce(500f, other.transform.position,0.2f);
        }
    }

    public void BaseFounded() {
        BasesDescubiertas += 1;
        Debug.Log("Player: Nueva base descubierta");
        Debug.Log("Player: Bases Descubiertas = " + BasesDescubiertas);
        
        if (BasesDescubiertas == 4) {
            Debug.Log("Fin del juego");
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb.AddRelativeForce(Vector3.forward * Time.deltaTime * Aceleration);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.AddRelativeForce(-Vector3.forward * Time.deltaTime * Aceleration);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddTorque(Vector3.up * Time.deltaTime * TurnAceleration);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.AddTorque(-Vector3.up * Time.deltaTime * TurnAceleration);
        }
        if (Input.GetKey(KeyCode.R))
        {
            rb.MovePosition(Vector3.zero);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            //if (lastShot >= fireRate)
            //{
                Shoot();
            //    lastShot = Random.Range(-1.5f, 0f);
            //}
            //lastShot += Time.deltaTime;

        }
    }
    protected void LateUpdate()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
    }
}
