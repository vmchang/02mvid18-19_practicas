﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public enum InputType2 { NONE, A , R }

public class GamePadButton : UIBehaviour
{
    #region STATIC
    static InputType2 input1 = InputType2.NONE;
    static InputType2 input2 = InputType2.NONE;
    public static bool GetInput(InputType2 input)
    {
        return input1 == input || input2 == input;
    }
    #endregion

    Image image;
    public InputType2 input;

    public void updateBtn(bool pushed)
    {
        changeColor(pushed);

        if (pushed)
        {
            if (input1 == InputType2.NONE) input1 = input;
            else if (input2 == InputType2.NONE) input2 = input;
        }
        else
        {
            if (input1 == input) input1 = InputType2.NONE;
            else if (input2 == input) input2 = InputType2.NONE;
        }
    }

    void changeColor(bool pushed)
    {
        image.color = pushed ? new Color(0, 0, 0, 1) : new Color(0, 0, 0, 0.3f);
    }

    protected override void Start()
    {
        image = GetComponent<Image>();
    }
}
